/* 
 * This file is part of the  distribution https://git.mosad.xyz/localhorst/ParallelTrafficRouter_FreeRTOS
 * Copyright (c) 2019 Hendrik Schutter.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include "demo.h"
#include "FreeRTOS.h"

NODE_t* getStartNode();

NODE_t* getStartNode() {

    static NODE_t* start;
    static NODE_t* g;
    static NODE_t* f;
    static NODE_t* e;
    static NODE_t* d;
    static NODE_t* c;
    static NODE_t* b;
    static NODE_t* a;
    static NODE_t* end;

    start = (NODE_t*) pvPortMalloc(sizeof(NODE_t));
    a = (NODE_t*) pvPortMalloc(sizeof(NODE_t));
    b = (NODE_t*) pvPortMalloc(sizeof(NODE_t));
    c = (NODE_t*) pvPortMalloc(sizeof(NODE_t));
    d = (NODE_t*) pvPortMalloc(sizeof(NODE_t));
    e = (NODE_t*) pvPortMalloc(sizeof(NODE_t));
    f = (NODE_t*) pvPortMalloc(sizeof(NODE_t));
    g = (NODE_t*) pvPortMalloc(sizeof(NODE_t));
    end = (NODE_t*) pvPortMalloc(sizeof(NODE_t));

    end->id = 8;
    end->nodescount = 0;

    g->id = 7;
    g->nodescount = 0;

    f->id = 6;
    f->nodescount = 1;
    f->nodesnext[0] = end;
    f->nodescost[0] = 7;

    e->id = 5;
    e->nodescount = 0;

    d->id = 4;
    d->nodescount = 0;

    c->id = 3;
    c->nodescount = 1;
    c->nodesnext[0] = end;
    c->nodescost[0] = 3;

    b->id = 2;
    b->nodescount = 3;
    b->nodesnext[0] = e;
    b->nodescost[0] = 11;
    b->nodesnext[1] = f;
    b->nodescost[1] = 42;
    b->nodesnext[2] = g;
    b->nodescost[2] = 8;

    a->id = 1;
    a->nodescount = 2;
    a->nodesnext[0] = c;
    a->nodescost[0] = 15;
    a->nodesnext[1] = d;
    a->nodescost[1] = 27;

    start->id = 0;
    start->nodescount = 2;
    start->nodesnext[0] = a;
    start->nodescost[0] = 72;
    start->nodesnext[1] = b;
    start->nodescost[1] = 32;

    return start;
}
