/* 
 * This file is part of the  distribution https://git.mosad.xyz/localhorst/ParallelTrafficRouter_FreeRTOS
 * Copyright (c) 2019 Hendrik Schutter.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEMO_H
#define DEMO_H

#define MAX_CHILDREN 3
#define MAX_DEPTH 4

typedef struct node NODE_t;
typedef struct threadData THREAD_DATA_t;
typedef struct bestPath BESTPATH_t;


extern NODE_t* getStartNode();

struct node {
    int id; //identifier for this node
    int nodescount; //how many children nodes
    NODE_t* nodesnext[MAX_CHILDREN]; //array of children nodes
    int nodescost[MAX_CHILDREN]; //array of cost per children node
};


struct threadData {
    int depth; //depth of the next node, e.g. start --> 0
    NODE_t* node; //next node to go
    int cost; //cost for the next node
    int predecessors[MAX_DEPTH]; // array with all predecessor
    BESTPATH_t* bestpath;
};


struct bestPath {
    int lowestCost;
    int predecessorsBestPath[MAX_DEPTH]; // array with all predecessor of the best path
    int depth; //depth of the best path, aka. how many nodes
};

#endif /* DEMO_H */
