# ParallelTrafficRouter_FreeRTOS

Example of a parallel algorithm in a FreeRTOS simulation

## Build requirements

### openSuse Tumbleweed

zypper install git make gcc-32bit

### Ubuntu 18.04

apt-get install git make gcc gcc-multilib g++-multilib

### Microsoft Windows

[FreeRTOS site for the Windows port](https://www.freertos.org/FreeRTOS-Windows-Simulator-Emulator-for-Visual-Studio-and-Eclipse-MingW.html)

## Build 

* git clone https://git.mosad.xyz/localhorst/ParallelTrafficRouter_FreeRTOS.git
* cd ParallelTrafficRouter_FreeRTOS
* make

## Execute
* ./FreeRTOS-Demo

### Using freeRTOS simulator fork from [https://github.com/Muriukidavid/FreeRTOS-Sim](https://github.com/Muriukidavid/FreeRTOS-Sim)






